home_name := $(wildcard home/*)
config_name := $(wildcard config/*)
nixos_name := $(wildcard nixos/*)
script_name := $(wildcard scripts/*)

.PHONY: home config nixos all scripts update

all: home config scripts
home: $(home_name)
config: $(config_name)
nixos: $(nixos_name)
scripts: $(script_name)

$(home_name):
	cp -i home/$< ${HOME}/.$<

$(config_name):
	cp -ir config/$< ${HOME}/.config/$<

$(nixos_name):
	sudo cp nixos/$< /etc/nixos/$<
	sudo nixos-rebuild switch

$(script_name):
	cp scripts/$< ${HOME}/.local/bin/$<
	chmod +x ${HOME}/.loacl/bin/$<
