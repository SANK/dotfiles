#!/bin/sh

zone() {
	NOW=$(TZ=$1 date '+%H')
	COLOUR=$([ $NOW -ge 13 ] && [ $NOW -le 23 ] && echo 4 || echo 234)
	TIME=$(TZ=$1 date '+%H:%M')
	echo "#[fg=colour${COLOUR}]${TIME}#[fg=default]"
}

echo "SW $(zone 'Europe/Stockholm') US $(zone 'America/New_York') FI $(zone 'Asia/Manila') TH $(zone 'Asia/Bangkok')"
