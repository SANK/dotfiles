{ config, pkgs, lib, fetchurl, fetchgit, ... }:

let list = map (x: x + 1714) (builtins.genList (x: x + 1) (1764 - 1714));
    pkgsUnstable = import (fetchTarball https://github.com/NixOS/nixpkgs-channels/archive/nixos-unstable.tar.gz) { };
    pkgsMaster = import (fetchTarball https://github.com/NixOS/nixpkgs/archive/master.tar.gz){ };
    hsEnv = pkgs.haskellPackages.ghcWithPackages (self : with self; [
      ghc
      ghc-mod
      #hdevtools
      pointful
      pointfree
      djinn
      hasktags
      happy
      hlint
      hoogle
      ghcid
      #goa
      #lambdabot
      #zot
      lhs2tex
    ]);
    # idris = pkgsMaster.idrisPackages.with-packages (with pkgsMaster.idrisPackages; [
    # idris = pkgs.idrisPackages.with-packages (with pkgsMaster.idrisPackages; [
    #    base prelude contrib effects pruviloj
    # ]);
in {
  imports =
    [
      ./hardware-configuration.nix
    ];

  # Use the GRUB 2 boot loader.
  boot = {
    cleanTmpDir = true;
    loader = {
      grub.enable = true;
      grub.version = 2;
      # grub.efiSupport = true;
      # grub.efiInstallAsRemovable = true;
      # efi.efiSysMountPoint = "/boot/efi";
      grub.device = "/dev/sda"; # or "nodev" for efi only
    };
  # Define on which hard drive you want to install Grub.
    initrd.luks.devices = [{
      name = "root";
      device = "/dev/sda3";
      preLVM = true;
    }];
  };

  virtualisation.libvirtd.enable = true;

  # Set your time zone.
  time.timeZone = "Europe/Stockholm";
  powerManagement.enable = true;

  # List packages installed in system profile. To search by name, run:
  # $ nix-env -qaP | grep wget
  nixpkgs.config.allowUnfree = true;
  environment = {
    systemPackages = with pkgs; [
      manpages
      pgcli
      dmenu
      geoclue2
      alsa-firmware
      alsaUtils
      openssl
      kmix
    ];
  };

  nix = {
    autoOptimiseStore = true;
  };

  programs = {
    adb.enable = true;
    bash.enableCompletion = true;
    thefuck.enable = true;
  };
  sound.mediaKeys.enable = true;
  hardware.opengl.driSupport32Bit = true;
  hardware.pulseaudio.support32Bit = true;

  fonts = {
    enableFontDir = true;
    enableGhostscriptFonts = true;
    fontconfig = {
      defaultFonts.monospace = [ "Fira Mono" ];
      defaultFonts.sansSerif = [ "Fira Sans" ];
      ultimate.enable = true;
    };
    fonts = with pkgs; [
      fira-code fira-mono font-droid noto-fonts-cjk
    ];
  };
  hardware.pulseaudio.enable = true;

  # programs.mtr.enable = true;
  # programs.gnupg.agent = { enable = true; enableSSHSupport = true; };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services = {
    redshift.provider = "geoclue2";
    redshift.enable = true;
    tor.enable = true;
    tor.client.enable = true;
    tor.client.privoxy.enable = true;
    geoclue2.enable = true;
    tlp.enable = true;
    # thermald.enable = true;
    openssh.enable = true;
    postgresql.enable = true;
    postgresql.package = pkgs.postgresql94;
    postgresql.authentication = lib.mkForce ''
    # Generated file; do not edit!
    # TYPE  DATABASE        USER            ADDRESS                 METHOD
    local   all             all                                     trust
    host    all             all             127.0.0.1/32            trust
    host    all             all             ::1/128                 trust
    '';
    xserver = {
      videoDrivers = [ "nvidiaLegacy340" ];
      libinput.enable = true;
      enable = true;
      layout = "us";
      desktopManager.xterm.enable = false;
      desktopManager.xfce.enable = true;
      # windowManager.dwm.enable = true;
      # windowManager.default = "dwm";
      displayManager.sessionCommands = ''
      '';
      displayManager.auto = { enable = true; user = "pi"; };
      wacom.enable = true;
    };
    cron.enable = true;
    cron.systemCronJobs = [
    ];
  };

  environment.extraInit = ''
    export GDK_PIXBUF_MODULE_FILE="$(echo ${pkgs.librsvg.out}/lib/gdk-pixbuf-2.0/*/loaders.cache)";
  '';

  environment.etc."sxhkdrc" = {
    text = ''
    '';
    mode = "0444";
  };

  # Open ports in the firewall.
  networking = {
    firewall.allowedTCPPorts = list ++ [ 22 ];
    firewall.allowedUDPPorts = list ++ [ 22 ];
    hostName = "OpenTelecomsAPI";
    networkmanager.enable = true;
  };

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.extraUsers.pi = {
    isNormalUser = true;
    name = "pi";
    group = "users";
    createHome = true;
    home = "/home/pi";
    shell = "/run/current-system/sw/bin/bash";
    uid = 1000;
    extraGroups = [ "networkmanager" "wheel" "audio" "video" "adbusers" ];
    packages = with pkgs; [
      # EDITORS
        vim
      # TERMINAL
        tcpdump
        maildrop
        mailutils
        gnupg
        tmux
        rlwrap
        konsole
        htop
        tree
        neofetch
        nix-repl
        lolcat
        sl
        at
        exercism
        cmatrix
        espeak
        svox
        binutils
      # COMPILERS & INTERPRETERS
        # FUNCTIONAL
          erlangR20
          hsEnv
          lci
          scala
          beam.packages.erlangR20.elixir
          #idris
          gnuapl
          j
        # SCRIPT
          python2
          python3
          ruby
          php
          nodejs-8_x
        # LISP
          chicken
          clojure
          ccl
          chez
        # MISC
          clang
          gcc
          kotlin
          nasm
      # DEVELOPMENT TOOLS
        # PACKAGE MANAGERS
          sbt
          cabal-install
          stack
          rebar
          leiningen
          php71Packages.composer
          rustNightlyBin.cargo
          pkgsUnstable.rustup
          lispPackages.quicklisp
        # TO NIX
          egg2nix
          hex2nix
          npm2nix
          cabal2nix
          pypi2nix
        # BUILD TOOLS
          pkgconfig
          gnumake
          cmake
          xorriso
        # MISC
          docker
          nix.man
          universal-ctags
          inotify-tools
          clang-tools
          git
          jq 
          qemu
          nixops
          glxinfo
          ant
          androidsdk
      # FONTS
        fira-code
      # UTILITIES
        unzip
        unrar
        file
        zip
      # COMMUNICATION
        telegram-cli
        pkgsUnstable.weechat 
      # OFFICE
        zathura
      # MEDIA
        # VIDEO/AUDIO
          youtube-dl 
          mpd
          mpv
          mpc_cli
          ncmpc
        # GAMES
          bsdgames
          steam
          xonotic
          zeroad
      # GRAPHICS
        vulkan-loader
        gimp
        mypaint
        inkscape
      # AUDIO
        supercollider
        ardour
        sox
      # WEB & NETWORKING
        telnet
        curl 
        links 
        urlview 
        mutt 
        torsocks
        fetchmail 
        aria2 
        firefox-bin
        wget
        aircrack-ng
      # THEME
        numix-gtk-theme
        numix-icon-theme-circle
        material-icons
      # MISC
        librsvg
        cpp_ethereum
        xawtv
      # DESKTOP
        light
        xdotool
        rofi
        xclip 
        scrot
        devilspie2
        kdeconnect
        redshift
        wmctrl
        bar-xft
        xlibs.xbacklight
    ];
  };

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "17.09"; # Did you read the comment?

}
# vim: et
